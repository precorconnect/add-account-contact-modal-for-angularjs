import angular from 'angular';
import AddAccountContactModal from './addAccountContactModal';
import errorMessagesTemplate from './error-messages.html!text';
import 'angular-bootstrap';
import 'angular-messages';
import 'bootstrap/css/bootstrap.css!css'
import 'bootstrap'

angular
    .module(
        'addAccountContactModal.module',
        [
            'ui.bootstrap',
            'ngMessages'
        ]
    )
    .service(
        'addAccountContactModal',
        [
            '$modal',
            AddAccountContactModal
        ]
    )
    .run(
        [
            '$templateCache',
            $templateCache => {
                $templateCache.put(
                    'add-account-contact-modal/error-messages.html',
                    errorMessagesTemplate
                );
            }
        ]
    );