import angular from 'angular';
import SessionManager from 'session-manager';
import AccountContactServiceSdk,{AddAccountContactReq} from 'account-contact-service-sdk';
import Iso3166Sdk from 'iso-3166-sdk';

export default class Controller {

    _sessionManager:SessionManager;

    _$q;

    _$modalInstance;

    _accountContactServiceSdk:AccountContactServiceSdk;

    _iso3166Sdk:Iso3166Sdk;

    _countryList;

    _countrySubdivisionList = [];

    constructor(sessionManager:SessionManager,
                $q,
                $modalInstance,
                accountContactServiceSdk,
                iso3166Sdk:Iso3166Sdk,
                initialAddAccountContactReqData) {

        if (!sessionManager) {
            throw new TypeError('sessionManager required');
        }
        this._sessionManager = sessionManager;

        if (!$q) {
            throw new TypeError('$q required');
        }
        this._$q = $q;

        if (!$modalInstance) {
            throw new TypeError('$modalInstance required');
        }
        this._$modalInstance = $modalInstance;

        if (!accountContactServiceSdk) {
            throw new TypeError('accountContactServiceSdk required');
        }
        this._accountContactServiceSdk = accountContactServiceSdk;

        if (!iso3166Sdk) {
            throw  new TypeError('iso3166Sdk required');
        }
        this._iso3166Sdk = iso3166Sdk;
        $q(resolve =>
            resolve(iso3166Sdk.listCountries())
        ).then(countryList => {
            this._countryList = countryList;
        });

        if (!initialAddAccountContactReqData) {
            throw TypeError('initialAddAccountContactReqData required');
        }
        this._addAccountContactReqData = angular.copy(initialAddAccountContactReqData);

    }

    get addAccountContactReqData() {
        return this._addAccountContactReqData;
    }

    get countryList() {
        return this._countryList;
    }

    get countrySubdivisionList() {
        return this._countrySubdivisionList;
    }

    handleAddressCountryCodeChange() {

        // clear selected region
        this._addAccountContactReqData.regionIso31662Code = null;

        if (this._addAccountContactReqData.countryIso31661Alpha2Code) {

            this._$q(resolve =>
                resolve(
                    this._iso3166Sdk.listSubdivisionsWithAlpha2CountryCode(
                        this._addAccountContactReqData.countryIso31661Alpha2Code
                    )
                )
            ).then(countrySubdivisionList => {
                this._countrySubdivisionList = countrySubdivisionList;
            });

        }

    }

    cancel() {
        this._$modalInstance.dismiss();
    }

    saveAndClose(form) {
        if (form.$valid) {

            this
                ._sessionManager
                .getAccessToken()
                .then(
                    accessToken =>
                        this._accountContactServiceSdk
                            .addAccountContact(
                                new AddAccountContactReq(
                                    this._addAccountContactReqData.accountId,
                                    this._addAccountContactReqData.firstName,
                                    this._addAccountContactReqData.lastName,
                                    this._addAccountContactReqData.phoneNumber,
                                    this._addAccountContactReqData.emailAddress,
                                    this._addAccountContactReqData.countryIso31661Alpha2Code,
                                    this._addAccountContactReqData.regionIso31662Code
                                ),
                                accessToken
                            )
                )
                .then(
                    accountContactId =>
                        this._$modalInstance.close(accountContactId)
                );


        }
        else {
            form.$setSubmitted();
        }
    }
}

Controller.$inject = [
    'sessionManager',
    '$q',
    '$modalInstance',
    'accountContactServiceSdk',
    'iso3166Sdk',
    'initialAddAccountContactReqData'
];