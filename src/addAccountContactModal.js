import Controller from './controller';
import './default.css!css';
import template from './index.html!text';
import {AddAccountContactReq} from 'account-contact-service-sdk';

export default class AddAccountContactModal {

    _$modal;

    constructor($modal) {

        if (!$modal) {
            throw new TypeError('$modal required');
        }
        this._$modal = $modal;

    }

    /**
     * Shows the add account contact modal
     * @param {AddAccountContactReq} initialAddAccountContactReqData
     * @returns {Promise<string>} account contact id
     */
    show(initialAddAccountContactReqData:AddAccountContactReq):Promise<string> {

        let modalInstance = this._$modal.open({
            controller: Controller,
            controllerAs: 'controller',
            template: template,
            backdrop: 'static',
            resolve: {
                initialAddAccountContactReqData: function () {
                    return initialAddAccountContactReqData;
                }
            }
        });

        return modalInstance.result;
    }
}

AddAccountContactModal.$inject = [
    '$modal'
];