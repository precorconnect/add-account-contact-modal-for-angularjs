System.config({
  baseURL: "../",
  defaultJSExtensions: true,
  transpiler: "babel",
  babelOptions: {
    "optional": [
      "runtime",
      "optimisation.modules.system",
      "es7.decorators",
      "es7.classProperties"
    ]
  },
  paths: {
    "bitbucket:*": "jspm_packages/bitbucket/*",
    "github:*": "jspm_packages/github/*",
    "npm:*": "jspm_packages/npm/*"
  },

  map: {
    "account-contact-service-sdk": "bitbucket:precorconnect/account-contact-service-sdk-for-javascript@0.0.13",
    "angular": "github:angular/bower-angular@1.4.5",
    "angular-bootstrap": "github:angular-ui/bootstrap-bower@0.13.4",
    "angular-messages": "github:angular/bower-angular-messages@1.4.5",
    "angular-route": "github:angular/bower-angular-route@1.4.5",
    "babel": "npm:babel-core@5.8.25",
    "babel-runtime": "npm:babel-runtime@5.8.25",
    "bootstrap": "github:twbs/bootstrap@3.3.5",
    "core-js": "npm:core-js@1.2.0",
    "css": "github:systemjs/plugin-css@0.1.16",
    "iso-3166-sdk": "bitbucket:precorconnect/iso-3166-sdk-for-javascript@0.0.14",
    "session-manager": "bitbucket:precorconnect/session-manager-for-browsers@0.0.54",
    "text": "github:systemjs/plugin-text@0.0.2",
    "bitbucket:precorconnect/account-contact-service-sdk-for-javascript@0.0.13": {
      "aurelia-dependency-injection": "github:aurelia/dependency-injection@0.9.2",
      "aurelia-http-client": "github:aurelia/http-client@0.11.0"
    },
    "bitbucket:precorconnect/identity-service-sdk-for-javascript@0.0.111": {
      "aurelia-dependency-injection": "github:aurelia/dependency-injection@0.9.2",
      "aurelia-http-client": "github:aurelia/http-client@0.10.3"
    },
    "bitbucket:precorconnect/iso-3166-sdk-for-javascript@0.0.14": {
      "aurelia-dependency-injection": "github:aurelia/dependency-injection@0.9.2",
      "iso-3166-2": "npm:iso-3166-2@0.2.1"
    },
    "bitbucket:precorconnect/session-manager-for-browsers@0.0.54": {
      "aurelia-dependency-injection": "github:aurelia/dependency-injection@0.9.2",
      "identity-service-sdk": "bitbucket:precorconnect/identity-service-sdk-for-javascript@0.0.111",
      "localforage": "npm:localforage@1.2.10",
      "uri": "github:medialize/URI.js@1.16.1"
    },
    "github:angular/bower-angular-route@1.4.5": {
      "angular": "github:angular/bower-angular@1.4.5"
    },
    "github:aurelia/dependency-injection@0.9.2": {
      "aurelia-logging": "github:aurelia/logging@0.6.4",
      "aurelia-metadata": "github:aurelia/metadata@0.7.3",
      "core-js": "npm:core-js@0.9.18"
    },
    "github:aurelia/http-client@0.10.3": {
      "aurelia-path": "github:aurelia/path@0.8.1",
      "core-js": "npm:core-js@0.9.18"
    },
    "github:aurelia/http-client@0.11.0": {
      "aurelia-path": "github:aurelia/path@0.9.0",
      "core-js": "npm:core-js@0.9.18"
    },
    "github:aurelia/metadata@0.7.3": {
      "core-js": "npm:core-js@0.9.18"
    },
    "github:jspm/nodelibs-assert@0.1.0": {
      "assert": "npm:assert@1.3.0"
    },
    "github:jspm/nodelibs-path@0.1.0": {
      "path-browserify": "npm:path-browserify@0.0.0"
    },
    "github:jspm/nodelibs-process@0.1.2": {
      "process": "npm:process@0.11.2"
    },
    "github:jspm/nodelibs-util@0.1.0": {
      "util": "npm:util@0.10.3"
    },
    "github:twbs/bootstrap@3.3.5": {
      "jquery": "github:components/jquery@2.1.4"
    },
    "npm:asap@1.0.0": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:assert@1.3.0": {
      "util": "npm:util@0.10.3"
    },
    "npm:babel-runtime@5.8.25": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:core-js@0.9.18": {
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "process": "github:jspm/nodelibs-process@0.1.2",
      "systemjs-json": "github:systemjs/plugin-json@0.1.0"
    },
    "npm:core-js@1.2.0": {
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "process": "github:jspm/nodelibs-process@0.1.2",
      "systemjs-json": "github:systemjs/plugin-json@0.1.0"
    },
    "npm:inherits@2.0.1": {
      "util": "github:jspm/nodelibs-util@0.1.0"
    },
    "npm:localforage@1.2.10": {
      "path": "github:jspm/nodelibs-path@0.1.0",
      "process": "github:jspm/nodelibs-process@0.1.2",
      "promise": "npm:promise@5.0.0"
    },
    "npm:path-browserify@0.0.0": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:process@0.11.2": {
      "assert": "github:jspm/nodelibs-assert@0.1.0"
    },
    "npm:promise@5.0.0": {
      "asap": "npm:asap@1.0.0"
    },
    "npm:util@0.10.3": {
      "inherits": "npm:inherits@2.0.1",
      "process": "github:jspm/nodelibs-process@0.1.2"
    }
  }
});
