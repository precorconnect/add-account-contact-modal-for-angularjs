import angular from 'angular';
import '../src/module';
import RouteConfig from './routeConfig';
import SessionManager,{SessionManagerConfig} from 'session-manager';
import AccountContactServiceSdk,{AccountContactServiceSdkConfig} from 'account-contact-service-sdk';
import Iso3166Sdk from 'iso-3166-sdk';
import 'angular-route';

const precorConnectApiBaseUrl = 'https://api-dev.precorconnect.com';

const sessionManager =
    new SessionManager(
        new SessionManagerConfig(
            precorConnectApiBaseUrl/* identityServiceBaseUrl */,
            'https://precor.oktapreview.com/app/template_saml_2_0/exk5cmdj3pY2eT5JU0h7/sso/saml'/* loginUrl */,
            'https://dev.precorconnect.com/customer/account/logout/'/* logoutUrl*/,
            30000/* accessTokenRefreshInterval */
        )
    );

const accountContactServiceSdk =
    new AccountContactServiceSdk(
        new AccountContactServiceSdkConfig(
            precorConnectApiBaseUrl
        )
    );

angular
    .module(
        'exampleApp.module',
        [
            'ngRoute',
            'addAccountContactModal.module'
        ]
    )
    .constant(
        'sessionManager',
        sessionManager
    )
    .constant(
        'accountContactServiceSdk',
        accountContactServiceSdk
    )
    .constant(
        'iso3166Sdk',
        new Iso3166Sdk()
    )
    .config(
        [
            '$routeProvider',
            $routeProvider => new RouteConfig($routeProvider)
        ]
    );
