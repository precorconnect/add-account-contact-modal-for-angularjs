import addAccountContactTemplate from './add-account-contact.html!text';
import AddAccountContactController from './addAccountContactController';

export default class RouteConfig {

    constructor($routeProvider) {

        $routeProvider
            .when
            (
                '/',
                {
                    template: addAccountContactTemplate,
                    controller: AddAccountContactController,
                    controllerAs: 'controller'
                }
            );

    }

}

RouteConfig.$inject = [
    '$routeProvider'
];