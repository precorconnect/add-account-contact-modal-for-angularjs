import SessionManager from 'session-manager';
import AccountContactServiceSdk from 'account-contact-service-sdk';

export default class AddAccountContactController {

    _sessionManager:SessionManager;

    _addAccountContactModal;

    _accountContactServiceSdk:AccountContactServiceSdk;

    _initialAddAccountContactReqData = {};

    _accountContact;

    constructor(sessionManager:SessionManager,
                addAccountContactModal,
                accountContactServiceSdk:AccountContactServiceSdk) {

        if (!sessionManager) {
            throw new TypeError('sessionManager required');
        }
        this._sessionManager = sessionManager;

        if (!addAccountContactModal) {
            throw new TypeError('addAccountContactModal required');
        }
        this._addAccountContactModal = addAccountContactModal;

        if (!accountContactServiceSdk) {
            throw new TypeError('accountContactServiceSdk required');
        }
        this._accountContactServiceSdk = accountContactServiceSdk;

    }

    get initialAddAccountContactReqData() {
        return this._initialAddAccountContactReqData;
    }

    get accountContact() {
        return this._accountContact;
    }

    showAddAccountContactModal() {

        this._addAccountContactModal
            .show(this._initialAddAccountContactReqData)
            .then(accountContactId => {
                this._accountContact = {id: accountContactId};
            })
            .then(() => this._sessionManager.getAccessToken())
            .then(accessToken =>
                this._accountContactServiceSdk
                    .getAccountContactWithId(
                        this._accountContact.id,
                        accessToken
                    )
            )
            .then(accountContact => {
                    this._accountContact = accountContact;
                }
            );

    }

}


AddAccountContactController.$inject = [
    'sessionManager',
    'addAccountContactModal',
    'accountContactServiceSdk'
];
