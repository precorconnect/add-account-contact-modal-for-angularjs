## Description
Add account contact modal for AngularJS.

## Example
refer to the [example app](example) for working example code.

## Setup

**install via jspm**  
```shell
jspm install bitbucket:precorconnect/add-account-contact-modal-for-angularjs
``` 

**import & wire up**
```js
import 'add-account-contact-modal-for-angularjs';

angular.module(
            "app",
            ["add-account-contact-modal.module"]
        )
        // ensure dependencies available in container
        .constant(
            'sessionManager', 
            sessionManager
            /*see https://bitbucket.org/precorconnect/session-manager-for-browsers*/
        )
        .constant(
            'accountContactServiceSdk',
            accountContactServiceSdk
            /*see https://bitbucket.org/precorconnect/account-contact-service-sdk-for-javascript*/
        )        
        .constant(
            'iso3166Sdk',
            iso3166Sdk
            /*see https://bitbucket.org/precorconnect/iso-3166-sdk-for-javascript*/
        );
```